import { Injectable, Logger } from '@nestjs/common';
import { MailService } from './mail/mail.service';
import { Client } from './user/user.entity';

@Injectable()
export class AppService {
  constructor(private mailService: MailService) {}

  getHello(): string {
    console.log('Get Hello');

    return 'Hello World!';
  }

  async sendClientConfirmation(client: Client) {
    console.log('Service/sendClientConfirmation');
    console.log('Service/client: ', client);
    await this.mailService.sendClientConfirmation(client);
  }
}
