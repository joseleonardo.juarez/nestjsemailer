export interface Client {
  email: string;
  name: string;
  claims: string[];
}
