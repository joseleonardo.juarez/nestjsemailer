import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { Client } from './user/user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    console.log('Controller/getHello');

    return this.appService.getHello();
  }

  @Post()
  postEmail(@Body() client: Client): any {
    console.log('Controller/postEmail');
    console.log('Controller/postEmail/client:', client);
    return this.appService.sendClientConfirmation(client);
  }
}
