import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Client } from 'src/user/user.entity';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendClientConfirmation(client: Client) {
    console.log('MailService/sendClientConfirmation/client: ', client);

    await this.mailerService.sendMail({
      to: client.email,
      subject: 'Gracias por tu compra',
      template: 'email-welcome-es',
      context: {
        name: client.name,
        claims: client.claims,
      },
    });
  }
}
